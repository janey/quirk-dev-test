<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Form extends CI_Controller {

    public function __construct() 
    {
        parent::__construct();
        $this->load->model('form_model');
        $this->load->library('form_validation');
        $this->load->helper('form');
    }

    public function index()
    {
        $this->load->view('quirk_form');
    }

    public function validate()
    {
        $this->form_validation->set_rules('fullname', 'Full Name', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('country', 'Country', 'trim|required');
        $this->form_validation->set_rules('day', 'Day', 'trim|required');
        $this->form_validation->set_rules('month','month', 'trim|required');
        $this->form_validation->set_rules('year','Date of Birth', 'trim|required|callback_valid_date');
        $this->form_validation->set_rules('rating', 'Star rating', 'trim|required');
        $this->form_validation->set_rules('accept_terms', 'Terms & Conditions', 'callback_terms_check');
        $this->form_validation->set_error_delimiters('', '');


        if ( $this->form_validation->run() === FALSE ) {

            if ( $this->input->post('nojs') == TRUE ) {

                $this->load->view('quirk_form');

            } else {

                $errors = array(
                    'status' => 0,
                    'fullname' => form_error('fullname'),
                    'email' => form_error('email'),
                    'country' => form_error('country'),
                    'dob' => form_error('year'),
                    'rating' => form_error('rating'),
                    'accept_terms' => form_error('accept_terms')
                );

                echo json_encode($errors);
            }

        } else {

            $this->form_model->create();

            $nicename = array(
                'Name'=>  $this->input->post('fullname'),
                'Email address'=> $this->input->post('email'),
                'Country'=> $this->config->item($this->input->post('country'), 'countries_list'),
                'Date of Birth'=> date("Y-m-d", strtotime($this->input->post('day').'-'.$this->input->post('month').'-'.$this->input->post('year'))),
                'Star Rating'=> $this->input->post('rating'),
                'Accepted Terms?'=> $this->input->post('accept_terms')
            );

            foreach ($nicename as $key => $value) {
                $response .= '<div class="success_label">'.$key.'</div>';
                $response .= '<div class="success_field">'.$value.'</div>';
            }

            if ( $this->input->post('nojs') == TRUE ) {

                $data['response'] = $response;

                $this->load->view('quirk_form', $data);

            } else {

                echo json_encode(array('status' => 1, 'response' => $response));
            }
            

        }
    }

    public function valid_date()
    {
         $day     = $this->input->post('day');
         $month   = $this->input->post('month');
         $year    = $this->input->post('year');

        if (!$day || !$month || !$year)
        {
            $this->form_validation->set_message('valid_date', 'Please make sure you have entered all date fields.');
            return FALSE;

        } elseif (date('Y') < $year) {

            $this->form_validation->set_message('valid_date', 'You have entered a future day, You arent born yet.');
            return FALSE;

        } elseif (!checkdate($month,  $day, $year)) {

            $this->form_validation->set_message('valid_date', 'You have entered an invalid date.');
            return FALSE;

        } elseif (mktime(0, 0, 0, $month, $day, $year) > strtotime('-18 years')) {

            $this->form_validation->set_message('valid_date', 'You must be 18 years old to sign up.');
            return false;

        }
            
        return TRUE; 
    }

    public function terms_check()
    {
        if ($this->input->post('accept_terms') != 'Yes' )
        {
            $this->form_validation->set_message('terms_check', 'Please accept Terms & Conditions');
            return FALSE;

        }
            
        return TRUE; 
    }
}

/* End of file form.php */
/* Location: ./application/controllers/form.php */
