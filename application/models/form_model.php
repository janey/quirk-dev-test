<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Form_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
     
    public function create()
    {

        $data = array(
            'fullname'=>  $this->input->post('fullname'),
            'email'=> $this->input->post('email'),
            'country'=> $this->config->item($this->input->post('country'), 'countries_list'),
            'dob'=> date("Y-m-d", strtotime($this->input->post('day').'-'.$this->input->post('month').'-'.$this->input->post('year'))),
            'rating'=> $this->input->post('rating'),
            'accept_terms'=> $this->input->post('accept_terms')
        );

        $this->db->insert('quirk_form_register', $data);
    }
     
}
 
?>
