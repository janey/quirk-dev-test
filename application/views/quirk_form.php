<?php $this->load->view('partials/header_view'); ?>

<section class="form-container">
    <section class="form-fields">
        <?php echo form_open('form/validate', array('id'=>'registerForm')) ?>

            <h2>Sign up form header (H2)</h2>

            <div class="no-js"><?php echo form_hidden('nojs', TRUE); ?></div>

            <div class="fullname">
                <?php echo form_label('Full Name<abbr title="required">*</abbr>:', 'fullname'); ?>
                <?php echo form_input(array('name'=> 'fullname','id'=> 'fullname', 'value' => set_value('fullname'))); ?>
                <?php echo form_error('fullname', '<span class="error msg">', '</span>'); ?>
            </div>

            <div class="email">
                <?php echo form_label('Email Address<abbr title="required">*</abbr>:', 'email'); ?>
                <?php echo form_input(array('name'=> 'email', 'id'=> 'email', 'value' => set_value('email'))); ?>
                <?php echo form_error('email', '<span class="error msg">', '</span>'); ?>
            </div>

            <div class="country">
                <?php echo form_label('Country<abbr title="required">*</abbr>:', 'country'); ?>
                <?php echo form_dropdown('country', config_item('countries_list'), set_value('country'), 'id="country"' ); ?>
                <?php echo form_error('country', '<span class="error msg">', '</span>'); ?>
            </div>

            <div class="dob">
                <?php echo form_fieldset('Date of birth<abbr title="required">*</abbr>:'); ?>
                    <p><?php echo form_input(array('name'=> 'day', 'id'=> 'day', 'maxlength'=>'2', 'value' => set_value('day'), 'placeholder' => 'DD')); ?></p>
                    <p><?php echo form_input(array('name'=> 'month', 'id'=> 'month', 'maxlength'=>'2', 'value' => set_value('month'), 'placeholder' => 'MM')); ?></p>
                    <p><?php echo form_input(array('name'=> 'year', 'id'=> 'year', 'maxlength'=>'4', 'value' => set_value('year'), 'placeholder' => 'YYYY')); ?></p>
                    <?php echo form_error('year', '<span class="error msg">', '</span>'); ?>
                <?php echo form_fieldset_close(); ?>
            </div>

            <div class="rating">
                <?php echo form_label('Give me a star rating<abbr title="required">*</abbr>', 'rating', array('class' => 'checkbox-label')); ?>
                <span class="star-rating">
                <?php echo form_radio(array('name' => 'rating', 'id' => 'rating', 'value' => '1')); ?><i></i>
                <?php echo form_radio(array('name' => 'rating', 'id' => 'rating', 'value' => '2')); ?><i></i>
                <?php echo form_radio(array('name' => 'rating', 'id' => 'rating', 'value' => '3')); ?><i></i> 
                <?php echo form_radio(array('name' => 'rating', 'id' => 'rating', 'value' => '4')); ?><i></i> 
                <?php echo form_radio(array('name' => 'rating', 'id' => 'rating', 'value' => '5')); ?><i></i> 
                </span>
                <?php echo form_error('rating', '<span class="error msg">', '</span>'); ?>
            </div>

            <div class="accept_terms">
                <?php echo form_checkbox(array('name' => 'accept_terms', 'id' => 'accept_terms', 'value' => 'Yes')); ?> 
                <?php echo form_label('I accept the terms and conditions<abbr title="required">*</abbr> ', 'accept_terms', array('class' => 'checkbox-label')); ?>
                <?php echo form_error('accept_terms', '<span class="error msg">', '</span>'); ?>
            </div>

            <?php echo form_button(array('name' => 'form_submit', 'id' => 'form_submit', 'value' => 'true', 'type' => 'submit', 'content' => 'Sign me up')); ?>

        <?php echo form_close(); ?>

    </section>

    <section class="form-response <?php if (!isset($response)) { ?>visually-hidden<?php }; ?>">
        <div class="no-js">
        <?php if (isset($response)) { 
            echo '<h2>Sign up success! </h2>';
            echo $response; 
        } ?>
        </div>
        <div class="bubble"></div>
    </section>

</section>

<?php $this->load->view('partials/footer_view'); ?> 
