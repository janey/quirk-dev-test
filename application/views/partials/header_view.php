<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content='IE=edge,chrome=1' http-equiv='X-UA-Compatible' />
    <title>Quirk - Dev Test - Jane Hollander</title>
    <meta content='Quirk Dev Test' name='description' />
    <meta content='width=device-width, initial-scale=1.0, minimal-ui, user-scalable=0' name='viewport' />

    <link href="<?php echo base_url();?>css/normalize.css" media="screen, projection" rel="stylesheet" type="text/css" />
  	<link href="<?php echo base_url();?>css/style.css" media="screen, projection" rel="stylesheet" type="text/css" />

</head>
</head>
<body class="no-svg">

    <header>
        <div class="site-logo">
            <a href="<?php echo base_url();?>">
                <span class= "visually-hidden">Quirk</span>
            </a>
        </div>
        <nav class="site-navigation">
            <div class="menu-toggle closed">Menu</div>
            <ul>
                <li><a href="<?php echo base_url();?>">Work</a></li>
                <li><a href="<?php echo base_url();?>">Services</a></li>
                <li><a href="<?php echo base_url();?>">Resources</a></li>
                <li><a href="<?php echo base_url();?>">Company</a></li>
            </ul>
        </nav>
    </header>

    <div class="content">
