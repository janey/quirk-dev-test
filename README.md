# Quirk PHP Developer Assignment
Completed by: Jane Hollander

Date: 9 July 2015

## Setting up Config
* Database:
    * Create new Database in MySQL.
    * Import /quirk_form.sql into new Database.
    * Edit /application/config/database.php change following fields to connect to your newly created database.
        * $db['default']['hostname']
        * $db['default']['username']
        * $db['default']['password']
        * $db['default']['database']
   
* Edit /application/config/config.php 
    * Change $config['base_url'] to your localhost alias.

## Assessment notes
 * RAW Frontend HTML found under /HTML
 * Mobile First approach for Frontend
 * CSS3 Animation used on success block (bouncing ball)
 * @font-face use to include Roboto Fonts.
 * Extra
    * Fallback in place for no javascript.