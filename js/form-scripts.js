$(function(){
    $('.no-js').remove();
});

$(function(){
    $('#registerForm').submit(function(){
        $.post($('#registerForm').attr('action'),$('#registerForm').serialize(), function(json){

            $('.form-response').empty();
            $('.error').removeClass('error');
            $('.msg').remove();

            if (json.status == 0){
                $('.form-response').addClass('visually-hidden');
                $.each( json, function( field, error ){
                    if(error){
                        $('.'+field).addClass('error').append('<span class="msg">'+error+'</span>');
                    }
                });
                $('.form-response').html(json);
            } else {
                $('.form-response').removeClass('visually-hidden');
                $('.error').empty();
                $('.form-response').html('<h2>Sign Up Success</h2>'+json.response);
                $('.form-response').append('<div class="bubble"></div>');
            }

        }, 'json');
        return false;
    });
});


$('.menu-toggle').click(function(){
    $('.menu-toggle').toggleClass('closed');
    $('.site-navigation ul').slideToggle();
});
