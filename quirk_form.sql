-- phpMyAdmin SQL Dump
-- version 4.4.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 08, 2015 at 05:13 PM
-- Server version: 5.5.42
-- PHP Version: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `quirk_form`
--

-- --------------------------------------------------------

--
-- Table structure for table `quirk_form_register`
--

CREATE TABLE `quirk_form_register` (
  `id` int(11) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `email` varchar(120) NOT NULL,
  `country` varchar(100) NOT NULL,
  `dob` date NOT NULL,
  `rating` int(1) NOT NULL,
  `accept_terms` varchar(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quirk_form_register`
--

INSERT INTO `quirk_form_register` (`id`, `fullname`, `email`, `country`, `dob`, `rating`, `accept_terms`) VALUES
(1, 'Jane Hollander', 'hollander.jane@gmail.com', 'South Africa', '1988-11-22', 5, 'Yes');


--
-- Indexes for dumped tables
--

--
-- Indexes for table `quirk_form_register`
--
ALTER TABLE `quirk_form_register`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `quirk_form_register`
--
ALTER TABLE `quirk_form_register`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;